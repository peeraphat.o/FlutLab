import 'dart:async';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime currentDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate)
      setState(() {
        currentDate = pickedDate;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DatePicker"),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(currentDate.toString()),
            RaisedButton(
              onPressed: () => _selectDate(context),
              child: Text('Select date'),
            ),
          ],
        ),
      ),
    );
  }
}

// import 'package:flutter/material.dart';
// import 'package:syncfusion_flutter_charts/charts.dart';
// import 'package:syncfusion_flutter_charts/sparkcharts.dart';

// void main() {
//   return runApp(_ChartApp());
// }

// class _ChartApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       theme: ThemeData(primarySwatch: Colors.blue),
//       home: _MyHomePage(),
//     );
//   }
// }

// class _MyHomePage extends StatefulWidget {
//   // ignore: prefer_const_constructors_in_immutables
//   _MyHomePage({Key? key}) : super(key: key);

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<_MyHomePage> {
//   List<_SalesData> data = [
//     _SalesData('Jan', 35),
//     _SalesData('Feb', 28),
//     _SalesData('Mar', 34),
//     _SalesData('Apr', 32),
//     _SalesData('May', 40)
//   ];
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: const Text('Syncfusion Flutter chart'),
//         ),
//         body: Column(children: [
//           //Initialize the chart widget
//           SfCartesianChart(
//               primaryXAxis: CategoryAxis(),
//               // Chart title
//               title: ChartTitle(text: 'Half yearly sales analysis'),
//               // Enable legend
//               legend: Legend(isVisible: true),
//               // Enable tooltip
//               tooltipBehavior: TooltipBehavior(enable: true),
//               series: <ChartSeries<_SalesData, String>>[
//                 LineSeries<_SalesData, String>(
//                     dataSource: data,
//                     xValueMapper: (_SalesData sales, _) => sales.year,
//                     yValueMapper: (_SalesData sales, _) => sales.sales,
//                     name: 'Sales',
//                     // Enable data label
//                     dataLabelSettings: DataLabelSettings(isVisible: true))
//               ]),
//           Expanded(
//             child: Padding(
//               padding: const EdgeInsets.all(8.0),
//               //Initialize the spark charts widget
//               child: SfSparkLineChart.custom(
//                 //Enable the trackball
//                 trackball: SparkChartTrackball(
//                     activationMode: SparkChartActivationMode.tap),
//                 //Enable marker
//                 marker: SparkChartMarker(
//                     displayMode: SparkChartMarkerDisplayMode.all),
//                 //Enable data label
//                 labelDisplayMode: SparkChartLabelDisplayMode.all,
//                 xValueMapper: (int index) => data[index].year,
//                 yValueMapper: (int index) => data[index].sales,
//                 dataCount: 5,
//               ),
//             ),
//           )
//         ]));
//   }
// }

// class _SalesData {
//   _SalesData(this.year, this.sales);

//   final String year;
//   final double sales;
// }



// import 'package:flutter/material.dart';

// void main() => runApp(new MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // return new MaterialApp(
//     //   title: 'Flutter Simple Login Demo',
//     //   theme: new ThemeData(
//     //     primarySwatch: Colors.blue
//     //   ),
//     //   home: new LoginPage(),
//     // );
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MyHomePage(),
//     );
//   }
// }
// class NavDrawer extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Drawer(
//       child: ListView(
//         padding: EdgeInsets.zero,
//         children: <Widget>[
//           DrawerHeader(
//             child: Text(
//               'Side menu',
//               style: TextStyle(color: Colors.white, fontSize: 25),
//             ),
//             decoration: BoxDecoration(
//                 color: Colors.green,
//                 image: DecorationImage(
//                     fit: BoxFit.fill,
//                     image: AssetImage('assets/images/cover.jpg'))),
//           ),
//           ListTile(
//             leading: Icon(Icons.input),
//             title: Text('Welcome'),
//             onTap: () => {Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginPage()))},
//           ),
//           ListTile(
//             leading: Icon(Icons.verified_user),
//             title: Text('Profile'),
//             onTap: () => {Navigator.of(context).pop()},
//           ),
//           ListTile(
//             leading: Icon(Icons.settings),
//             title: Text('Settings'),
//             onTap: () => {Navigator.of(context).pop()},
//           ),
//           ListTile(
//             leading: Icon(Icons.border_color),
//             title: Text('Feedback'),
//             onTap: () => {Navigator.of(context).pop()},
//           ),
//           ListTile(
//             leading: Icon(Icons.exit_to_app),
//             title: Text('Logout'),
//             onTap: () => {Navigator.of(context).pop()},
//           ),
//         ],
//       ),
//     );
//   }
// }

// class MyHomePage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       drawer: NavDrawer(),
//       appBar: AppBar(
//         title: Text('Side menu'),
//       ),
//       body: Center(
//         child: Text('Side Menu Tutorial'),
//       ),
//     );
//   }
// }

// class LoginPage extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => new _LoginPageState();
// }

// // Used for controlling whether the user is loggin or creating an account
// enum FormType {
//   login,
//   register
// }

// class _LoginPageState extends State<LoginPage> {

//   final TextEditingController _emailFilter = new TextEditingController();
//   final TextEditingController _passwordFilter = new TextEditingController();
//   String _email = "";
//   String _password = "";
//   FormType _form = FormType.login; // our default setting is to login, and we should switch to creating an account when the user chooses to

//   _LoginPageState() {
//     _emailFilter.addListener(_emailListen);
//     _passwordFilter.addListener(_passwordListen);
//   }

//   void _emailListen() {
//     if (_emailFilter.text.isEmpty) {
//       _email = "";
//     } else {
//       _email = _emailFilter.text;
//     }
//   }

//   void _passwordListen() {
//     if (_passwordFilter.text.isEmpty) {
//       _password = "";
//     } else {
//       _password = _passwordFilter.text;
//     }
//   }

//   // Swap in between our two forms, registering and logging in
//   void _formChange () async {
//     setState(() {
//       if (_form == FormType.register) {
//         _form = FormType.login;
//       } else {
//         _form = FormType.register;
//       }
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       appBar: AppBar(
//           title: const Text('Welcome to Flutter'),
//         ),//_buildBar(context),
//       body: new Container(
//         padding: EdgeInsets.all(16.0),
//         child: new Column(
//           children: <Widget>[
//             _buildTextFields(),
//             _buildButtons(),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget _buildBar(BuildContext context) {
//     return new AppBar(
//       title: new Text("Simple Login Example"),
//       centerTitle: true,
//     );
//   }

//   Widget _buildTextFields() {
//     return new Container(
//       child: new Column(
//         children: <Widget>[
//           new Container(
//             child: new TextField(
//               controller: _emailFilter,
//               decoration: new InputDecoration(
//                 labelText: 'Email'
//               ),
//             ),
//           ),
//           new Container(
//             child: new TextField(
//               controller: _passwordFilter,
//               decoration: new InputDecoration(
//                 labelText: 'Password'
//               ),
//               obscureText: true,
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   Widget _buildButtons() {
//     if (_form == FormType.login) {
//       return new Container(
//         child: new Column(
//           children: <Widget>[
//             new RaisedButton(
//               child: new Text('Login'),
//               onPressed: _loginPressed,
//             ),
//             new FlatButton(
//               child: new Text('Dont have an account? Tap here to register.'),
//               onPressed: _formChange,
//             ),
//             new FlatButton(
//               child: new Text('Forgot Password?'),
//               onPressed: _passwordReset,
//             )
//           ],
//         ),
//       );
//     } else {
//       return new Container(
//         child: new Column(
//           children: <Widget>[
//             new RaisedButton(
//               child: new Text('Create an Account'),
//               onPressed: _createAccountPressed,
//             ),
//             new FlatButton(
//               child: new Text('Have an account? Click here to login.'),
//               onPressed: _formChange,
//             )
//           ],
//         ),
//       );
//     }
//   }

//   // These functions can self contain any user auth logic required, they all have access to _email and _password

//   void _loginPressed () {
//     print('The user wants to login with $_email and $_password');
//   }

//   void _createAccountPressed () {
//     print('The user wants to create an accoutn with $_email and $_password');

//   }

//   void _passwordReset () {
//     print("The user wants a password reset request sent to $_email");
//   }


// }